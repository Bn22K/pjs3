(function(){
    
    let sidebar = false
    let act = document.querySelector('#active-menu')
    act.addEventListener('mouseenter', function(e){
        e.stopPropagation()
        e.preventDefault()
        
        document.body.classList.add('has-sidebar')
        sidebar = true
    })

    document.body.addEventListener('click',function(){
        if(sidebar){
            document.body.classList.remove('has-sidebar')
        }

    })

})()